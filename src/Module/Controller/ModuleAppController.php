<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Controller;

use Paneric\BaseModule\Module\Action\App\CreateAppAction;
use Paneric\BaseModule\Module\Action\App\CreateMultipleAppAction;
use Paneric\BaseModule\Module\Action\App\DeleteAppAction;
use Paneric\BaseModule\Module\Action\App\DeleteMultipleAppAction;
use Paneric\BaseModule\Module\Action\App\GetAllAppAction;
use Paneric\BaseModule\Module\Action\App\GetAllPaginatedAppAction;
use Paneric\BaseModule\Module\Action\App\GetOneByIdAppAction;
use Paneric\BaseModule\Module\Action\App\UpdateAppAction;
use Paneric\BaseModule\Module\Action\App\UpdateMultipleAppAction;
use Paneric\CSRTriad\Controller\AppController;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class ModuleAppController extends AppController
{
    protected $routePrefix;

    public function __construct(Twig $twig, ConfigInterface $config)
    {
        parent::__construct($twig);

        $configValues = $config();

        $this->routePrefix = $configValues['route_prefix'];
    }

    public function showAll(
        Response $response,
        GetAllAppAction $action
    ): Response {
        return $this->render(
            $response,
            '@module/show_all.html.twig',
            $action()
        );
    }

    public function showAllPaginated(
        Request $request,
        Response $response,
        GetAllPaginatedAppAction $action,
        string $page = '1'
    ): Response {
        return $this->render(
            $response,
            '@module/show_all_paginated.html.twig',
            $action($request, $page)
        );
    }

    public function showOneById(
        Response $response,
        GetOneByIdAppAction $action,
        string $id
    ): Response {
        return $this->render(
            $response,
            '@module/show_one_by_id.html.twig',
            $action($id)
        );
    }

    public function add(
        Request $request,
        Response $response,
        CreateAppAction $action
    ): Response {
        $result = $action($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/add.html.twig',
            $result
        );
    }

    public function addMultiple(
        Request $request,
        Response $response,
        CreateMultipleAppAction $action
    ): Response {
        $result = $action($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/add_multiple.html.twig',
            $result
        );
    }

    public function edit(
        Request $request,
        Response $response,
        GetOneByIdAppAction $getOneByIdAction,
        UpdateAppAction $action,
        string $id
    ): Response {
        $result = $action($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/edit.html.twig',
            $getOneByIdAction($id)
        );
    }

    public function editMultiple(
        Request $request,
        Response $response,
        GetAllPaginatedAppAction $getAllPaginatedAction,
        UpdateMultipleAppAction $action,
        string $page = '1'
    ): Response {
        $result = $action($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/edit_multiple.html.twig',
            array_merge(
                $getAllPaginatedAction($request),
                ['page' => $page]
            )
        );
    }

    public function remove(
        Request $request,
        Response $response,
        DeleteAppAction $action,
        string $id
    ): Response {
        $result = $action($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/remove.html.twig',
            ['id' => $id]
        );
    }

    public function removeMultiple(
        Request $request,
        Response $response,
        GetAllPaginatedAppAction $getAllPaginatedAction,
        DeleteMultipleAppAction $action,
        string $page = '1'
    ): Response {
        $result = $action($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/remove_multiple.html.twig',
            array_merge(
                $getAllPaginatedAction($request),
                ['page' => $page]
            )
        );
    }
}
