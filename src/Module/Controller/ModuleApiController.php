<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Controller;

use Paneric\BaseModule\Module\Action\Api\CreateApiAction;
use Paneric\BaseModule\Module\Action\Api\CreateMultipleApiAction;
use Paneric\BaseModule\Module\Action\Api\DeleteApiAction;
use Paneric\BaseModule\Module\Action\Api\DeleteMultipleApiAction;
use Paneric\BaseModule\Module\Action\Api\GetAllApiAction;
use Paneric\BaseModule\Module\Action\Api\GetAllPaginatedApiAction;
use Paneric\BaseModule\Module\Action\Api\GetOneByIdApiAction;
use Paneric\BaseModule\Module\Action\Api\UpdateApiAction;
use Paneric\BaseModule\Module\Action\Api\UpdateMultipleApiAction;
use Paneric\CSRTriad\Controller\ApiController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ModuleApiController extends ApiController
{
    public function getAll(
        Request $request,
        Response $response,
        GetAllApiAction $action
    ): Response {
        return $this->jsonResponse(
            $response,
            $action($request),
            $action->getStatus()
        );
    }

    public function getAllPaginated(
        Request $request,
        Response $response,
        GetAllPaginatedApiAction $action,
        string $page = null
    ): Response {
        return $this->jsonResponse(
            $response,
            $action($request, $page),
            $action->getStatus()
        );
    }

    public function getOneById(
        Request $request,
        Response $response,
        GetOneByIdApiAction $action,
        string $id
    ): Response {
        return $this->jsonResponse(
            $response,
            $action($request, $id),
            $action->getStatus()
        );
    }

    public function create(
        Request $request,
        Response $response,
        CreateApiAction $action
    ): Response {
        return $this->jsonResponse(
            $response,
            $action($request),
            $action->getStatus(),
        );
    }

    public function createMultiple(
        Request $request,
        Response $response,
        CreateMultipleApiAction $action
    ): Response {
        return $this->jsonResponse(
            $response,
            $action($request),
            $action->getStatus(),
        );
    }

    public function update(
        Request $request,
        Response $response,
        UpdateApiAction $action,
        string $id
    ): Response {
        return $this->jsonResponse(
            $response,
            $action($request, $id),
            $action->getStatus(),
        );
    }

    public function updateMultiple(
        Request $request,
        Response $response,
        UpdateMultipleApiAction $action
    ): Response {
        return $this->jsonResponse(
            $response,
            $action($request),
            $action->getStatus(),
        );
    }

    public function delete(
        Request $request,
        Response $response,
        DeleteApiAction $action,
        string $id
    ): Response {
        return $this->jsonResponse(
            $response,
            $action($request, $id),
            $action->getStatus()
        );
    }

    public function deleteMultiple(
        Request $request,
        Response $response,
        DeleteMultipleApiAction $action
    ): Response {
        return $this->jsonResponse(
            $response,
            $action($request),
            $action->getStatus(),
        );
    }
}
