<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Controller;

use Paneric\BaseModule\Module\Action\Apc\CreateApcAction;
use Paneric\BaseModule\Module\Action\Apc\CreateMultipleApcAction;
use Paneric\BaseModule\Module\Action\Apc\DeleteApcAction;
use Paneric\BaseModule\Module\Action\Apc\DeleteMultipleApcAction;
use Paneric\BaseModule\Module\Action\Apc\GetAllApcAction;
use Paneric\BaseModule\Module\Action\Apc\GetAllPaginatedApcAction;
use Paneric\BaseModule\Module\Action\Apc\GetOneByIdApcAction;
use Paneric\BaseModule\Module\Action\Apc\UpdateApcAction;
use Paneric\BaseModule\Module\Action\Apc\UpdateMultipleApcAction;
use Paneric\CSRTriad\Controller\AppController;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class ModuleApcController extends AppController
{
    protected $session;
    protected $routePrefix;

    public function __construct(Twig $twig, ConfigInterface $config)
    {
        parent::__construct($twig);

        $configValues = $config();
        
        $this->routePrefix = $configValues['route_prefix'];
    }

    public function showAll(
        Request $request,
        Response $response,
        GetAllApcAction $action
    ): Response{
        return $this->render(
            $response,
            '@module/show_all.html.twig',
            $action($request)
        );
    }

    public function showAllPaginated(
        Request $request,
        Response $response,
        GetAllPaginatedApcAction $action,
        string $page = '1'
    ): Response {
        $result = $action($request, $page);
        return $this->render(
            $response,
            '@module/show_all_paginated.html.twig',
            $result
        );
    }

    public function showOneById(
        Request $request,
        Response $response,
        GetOneByIdApcAction $action,
        string $id
    ): Response {
        return $this->render(
            $response,
            '@module/show_one_by_id.html.twig',
            $action($request, $id)
        );
    }

    public function add(
        Request $request,
        Response $response,
        CreateApcAction $action
    ): Response {
        $result = $action($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/apc-' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/add.html.twig',
            $result
        );
    }

    public function addMultiple(
        Request $request,
        Response $response,
        CreateMultipleApcAction $action
    ): Response {
        $result = $action($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/apc-' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/add_multiple.html.twig',
            $result
        );
    }

    public function edit(
        Request $request,
        Response $response,
        GetOneByIdApcAction $getOneByIdAction,
        UpdateApcAction $action,
        string $id
    ): Response {
        $result = $action($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/apc-' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/edit.html.twig',
            array_merge($result, $getOneByIdAction($request, $id))
        );
    }

    public function editMultiple(
        Request $request,
        Response $response,
        GetAllPaginatedApcAction $getAllPaginatedAction,
        UpdateMultipleApcAction $action,
        string $page = '1'
    ): Response {
        $result1 = $action($request);
        if ($result1 === null) {
            return $this->redirect(
                $response,
                '/apc-' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        $result2 = $getAllPaginatedAction($request, $page);
        $action->adaptPaginationPath();

        return $this->render(
            $response,
            '@module/edit_multiple.html.twig',
            array_merge(
                $result1,
                $result2,
                ['page' => $page]
            )
        );
    }

    public function remove(
        Request $request,
        Response $response,
        DeleteApcAction $action,
        string $id
    ): Response {
        $result = $action($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/apc-' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/remove.html.twig',
            ['id' => $id]
        );
    }

    public function removeMultiple(
        Request $request,
        Response $response,
        GetAllPaginatedApcAction $getAllPaginatedAction,
        DeleteMultipleApcAction $action,
        string $page = '1'
    ): Response {
        $result1 = $action($request);
        if ($result1 === null) {
            return $this->redirect(
                $response,
                '/apc-' .  $this->routePrefix . 's/show-all-paginated',
                200
            );
        }

        $result2 = $getAllPaginatedAction($request, $page);
        $action->adaptPaginationPath();

        return $this->render(
            $response,
            '@module/remove_multiple.html.twig',
            array_merge(
                $result1,
                $result2,
                ['page' => $page]
            )
        );
    }
}
