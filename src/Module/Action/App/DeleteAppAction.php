<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\App;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class DeleteAppAction extends Action
{
    protected $adapter;

    protected $moduleNameSc;
    protected $deleteByCriteria;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['delete'];

        $this->adapter = $adapter;

        $this->moduleNameSc = $configValues['module_name_sc'];
        $this->deleteByCriteria = $configValues['delete_by_criteria'];
    }

    public function __invoke(Request $request, string $id): ?array
    {
        if ($request->getMethod() === 'POST') {
            $deleteByCriteria = $this->deleteByCriteria;

            if ($this->adapter->delete($deleteByCriteria(['id' => $id])) > 0) {
                return null;
            }

            $this->session->setFlash(['module_name_sc' => $this->moduleNameSc], 'value');
            $this->session->setFlash(['db_delete_error'], 'error');
        }

        $this->session->setFlash(['module_name_sc' => $this->moduleNameSc], 'value');
        $this->session->setFlash([$this->moduleNameSc . '_delete_warning'], 'warning');

        return [];
    }
}
