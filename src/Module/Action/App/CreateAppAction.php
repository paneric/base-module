<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\App;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class CreateAppAction extends Action
{
    protected $adapter;

    protected $moduleNameSc;
    protected $createUniqueCriteria;
    protected $daoClass;
    protected $dtoClass;

    protected $prefix;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['create'];

        $this->adapter = $adapter;

        $this->daoClass = $configValues['dao_class'];
        $this->dtoClass = $configValues['dto_class'];
        $this->createUniqueCriteria = $configValues['create_unique_criteria'];
        $this->moduleNameSc = $configValues['module_name_sc'];

        $this->prefix = $configValues['prefix'];
    }

    public function __invoke(Request $request): ?array
    {
        $attributes = $request->getParsedBody();

        $validationReport = $request->getAttribute('validation');

        if ($request->getMethod() === 'POST') {

            if (empty($validationReport[$this->dtoClass])) {
                $dao = new $this->daoClass();
                $dao->hydrate($attributes);

                $createUniqueCriteria = $this->createUniqueCriteria;

                if ($this->adapter->createUnique($createUniqueCriteria($attributes), $dao) !== null) {
                    return null;
                }

                $this->session->setFlash(['db_add_unique_error'], 'error');
            }
        }

        $this->session->setFlash(['module_name_sc' => $this->moduleNameSc], 'value');

        $dto = new $this->daoClass();
        $dto->hydrate($attributes ?? []);

        return [
            $this->prefix => $dto->convert(),
        ];
    }
}
