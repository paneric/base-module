<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\App;

use Paneric\CSRTriad\Action;
use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllPaginatedAppAction extends Action
{
    protected $adapter;

    protected $moduleNameSc;
    protected $findByCriteria;
    protected $orderBy;

    protected $prefix;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['get_all_paginated'];

        $this->adapter = $adapter;

        $this->findByCriteria = $configValues['find_by_criteria'];
        $this->orderBy = $configValues['order_by'];
        $this->moduleNameSc = $configValues['module_name_sc'];

        $this->prefix = $configValues['prefix'];
    }

    public function __invoke(Request $request, string $page = null): array
    {
        $pagination = $request->getAttribute('pagination');

        $this->session->setData($pagination, 'pagination');

        $this->session->setFlash(['module_name_sc' => $this->moduleNameSc], 'value');

        $findByCriteria = $this->findByCriteria;
        $orderBy = $this->orderBy;

        $local = strtolower($this->session->getData('local'));

        $collection = $this->adapter->findBy(
            $findByCriteria(),
            $orderBy($local),
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            $this->prefix . 's' => $this->arrangeObjectsCollectionById($collection, true),
        ];
    }
}
