<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\App;

use Paneric\CSRTriad\Action;
use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;

class GetAllAppAction extends Action
{
    protected $adapter;

    protected $moduleNameSc;
    protected $orderBy;

    protected $prefix;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['get_all'];

        $this->adapter = $adapter;

        $this->orderBy = $configValues['order_by'];
        $this->moduleNameSc = $configValues['module_name_sc'];

        $this->prefix = $configValues['prefix'];
    }

    public function __invoke(): array
    {
        $this->session->setFlash(['module_name_sc' => $this->moduleNameSc], 'value');

        $orderBy = $this->orderBy;

        $local = strtolower($this->session->getData('local'));

        $collection = $this->adapter->findBy(
            [],
            $orderBy($local)
        );

        return [
            $this->prefix . 's' => $this->arrangeObjectsCollectionById($collection, true)
        ];
    }
}
