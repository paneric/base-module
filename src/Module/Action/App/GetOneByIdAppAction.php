<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\App;

use Paneric\CSRTriad\Action;
use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;

class GetOneByIdAppAction extends Action
{
    protected $adapter;

    protected $findOneByCriteria;
    protected $moduleNameSc;

    protected $prefix;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['get_one_by_id'];

        $this->adapter = $adapter;

        $this->findOneByCriteria = $configValues['find_one_by_criteria'];
        $this->moduleNameSc = $configValues['module_name_sc'];

        $this->prefix = $configValues['prefix'];
    }

    public function __invoke(string $id): ?array
    {
        $findOneByCriteria = $this->findOneByCriteria;

        $dto = $this->adapter->findOneBy($findOneByCriteria($id));

        $this->session->setFlash(['module_name_sc' => $this->moduleNameSc], 'value');

        if ($dto ===  null) {
            return null;
        }

        return [
            $this->prefix => $dto->convert()
        ];
    }
}
