<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\App;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class DeleteMultipleAppAction extends Action
{
    protected $adapter;

    protected $moduleNameSc;
    protected $deleteByCriteria;
    protected $daoClass;
    protected $dtoClass;
    
    public function __construct(
        ModuleRepositoryInterface $adapter,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['delete_multiple'];

        $this->adapter = $adapter;

        $this->moduleNameSc = $configValues['module_name_sc'];
        $this->daoClass = $configValues['dao_class'];
        $this->dtoClass = $configValues['dto_class'];
        $this->deleteByCriteria = $configValues['delete_by_criteria'];
    }

    public function __invoke(Request $request): ?array
    {
        if ($request->getMethod() === 'POST') {
            $collection = $this->createCollectionFromAttributes(
                $request->getParsedBody(),
                $this->daoClass,
                true
            );

            $validationReport = $request->getAttribute('validation');

            if (empty($validationReport[$this->dtoClass])) {
                $deleteByCriteria = $this->deleteByCriteria;

                $collection = $this->adapter->deleteMultiple(
                    $deleteByCriteria($collection)
                );

                if (empty($collection)) {
                    return null;
                }

                $this->session->setFlash(['db_delete_multiple_error'], 'error');
            }
        }

        $this->session->setFlash(['module_name_sc' => $this->moduleNameSc], 'value');

        return [];
    }
}
















