<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\App;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class UpdateMultipleAppAction extends Action
{
    protected $adapter;

    protected $moduleNameSc;
    protected $daoClass;
    protected $dtoClass;
    protected $updateUniqueCriteria;
    protected $findByCriteria;

    protected $prefix;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['update_multiple'];

        $this->adapter = $adapter;

        $this->moduleNameSc = $configValues['module_name_sc'];
        $this->daoClass = $configValues['dao_class'];
        $this->dtoClass = $configValues['dto_class'];
        $this->updateUniqueCriteria = $configValues['update_unique_criteria'];
        $this->findByCriteria = $configValues['find_by_criteria'];

        $this->prefix = $configValues['prefix'];
    }

    public function __invoke(Request $request): ?array
    {
        if ($request->getMethod() === 'POST') {
            $collection = $this->createCollectionFromAttributes(
                $request->getParsedBody(),
                $this->daoClass,
                true
            );

            $validationReport = $request->getAttribute('validation');

            if (empty($validationReport[$this->dtoClass])) {
                $findByCriteria = $this->findByCriteria;
                $updateUniqueCriteria = $this->updateUniqueCriteria;

                $collection = $this->adapter->updateUniqueMultiple(
                    $findByCriteria($collection),
                    $updateUniqueCriteria($collection),
                    $collection
                );

                if (empty($collection)) {
                    return null;
                }

                $this->session->setFlash(['db_update_multiple_unique_error'], 'error');
            }
        }

        $this->session->setFlash(['module_name_sc' => $this->moduleNameSc], 'value');

        return [];
    }
}
