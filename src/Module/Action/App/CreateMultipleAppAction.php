<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\App;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class CreateMultipleAppAction extends Action
{
    protected $adapter;

    protected $moduleNameSc;
    protected $createUniqueCriteria;
    protected $daoClass;
    protected $dtoClass;

    protected $prefix;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $this->adapter = $adapter;

        $configValues = $config()['create_multiple'];

        $this->daoClass = $configValues['dao_class'];
        $this->dtoClass = $configValues['dto_class'];
        $this->createUniqueCriteria = $configValues['create_unique_criteria'];
        $this->moduleNameSc = $configValues['module_name_sc'];

        $this->prefix = $configValues['prefix'];
    }

    public function __invoke(Request $request): ?array
    {
        $collection = [];

        if ($request->getMethod() === 'POST') {
            $collection = $this->createCollectionFromAttributes(
                $request->getParsedBody(),
                $this->daoClass,
                true
            );

            $validationReport = $request->getAttribute('validation');

            if (empty($validationReport[$this->dtoClass])) {
                $createUniqueCriteria = $this->createUniqueCriteria;

                $collection = $this->adapter->createUniqueMultiple(
                    $createUniqueCriteria($collection),
                    $collection
                );

                if (empty($collection)) {
                    return null;
                }

                $this->session->setFlash(['db_create_multiple_error'], 'error');
            }
        }

        $this->session->setFlash(['module_name_sc' => $this->moduleNameSc], 'value');

        return [
            $this->prefix . 's' => $this->convertCollection($collection)
        ];
    }
}
