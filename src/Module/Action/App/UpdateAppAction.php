<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\App;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class UpdateAppAction extends Action
{
    protected $adapter;

    protected $moduleNameSc;
    protected $daoClass;
    protected $dtoClass;
    protected $updateUniqueCriteria;
    protected $findOneByCriteria;

    protected $prefix;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['update'];

        $this->adapter = $adapter;

        $this->moduleNameSc = $configValues['module_name_sc'];
        $this->daoClass = $configValues['dao_class'];
        $this->dtoClass = $configValues['dto_class'];
        $this->updateUniqueCriteria = $configValues['update_unique_criteria'];
        $this->findOneByCriteria = $configValues['find_one_by_criteria'];

        $this->prefix = $configValues['prefix'];
    }

    public function __invoke(Request $request, string $id): ?array
    {
        $findOneByCriteria = $this->findOneByCriteria;

        if ($request->getMethod() === 'POST') {
            $validationReport = $request->getAttribute('validation');

            if (empty($validationReport[$this->dtoClass])) {
                $dao = new $this->daoClass();
                $dao->hydrate(
                    $request->getParsedBody()
                );

                $updateUniqueCriteria = $this->updateUniqueCriteria;

                if ($this->adapter->updateUnique(
                        $findOneByCriteria($dao, $id),
                        $updateUniqueCriteria($id),
                        $dao
                    ) !== null) {
                    return null;
                }

                $this->session->setFlash(['db_update_unique_error'], 'error');
            }
        }

        $this->session->setFlash(['module_name_sc' => $this->moduleNameSc], 'value');

        return [];
    }
}
