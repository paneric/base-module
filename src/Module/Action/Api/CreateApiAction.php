<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Api;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class CreateApiAction extends Action
{
    protected $adapter;

    protected $createUniqueCriteria;
    protected $daoClass;
    protected $dtoClass;

    protected $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ConfigInterface $config
    ) {
        parent::__construct();

        $configValues = $config()['create'];

        $this->adapter = $adapter;

        $this->daoClass = $configValues['dao_class'];
        $this->dtoClass = $configValues['dto_class'];
        $this->createUniqueCriteria = $configValues['create_unique_criteria'];
    }

    public function __invoke(Request $request): ?array
    {
        if (!in_array('application/json;charset=utf-8', $request->getHeader('Content-type'), true)) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Invalid request type.'
            ];
        }

        $attributes = $request->getParsedBody();

        $validationReport = $request->getAttribute('validation');

        if (empty($validationReport[$this->dtoClass])) {
            $dao = new $this->daoClass();
            $dao->hydrate($attributes);

            $createUniqueCriteria = $this->createUniqueCriteria;

            if ($this->adapter->createUnique($createUniqueCriteria($attributes), $dao) !== null) {
                $this->status = 200;

                return [
                    'status' => $this->status
                ];
            }

            $dto = new $this->dtoClass();
            $dto->hydrate($attributes);

            $this->status = 200;

            return [
                'status' => $this->status,
                'error' => 'db_add_unique_error',
                'body' => $dto->convert()
            ];
        }

        $dto = new $this->dtoClass();
        $dto->hydrate($attributes);

        $this->status = 200;

        return [
            'status' => $this->status,
            'error' => $validationReport,
            'body' => $dto->convert()
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
