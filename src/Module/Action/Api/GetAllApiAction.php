<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Api;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllApiAction extends Action
{
    protected $adapter;

    protected $orderBy;

    protected $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ConfigInterface $config
    ) {
        parent::__construct();

        $configValues = $config()['get_all'];

        $this->adapter = $adapter;

        $this->orderBy = $configValues['order_by'];
    }

    public function __invoke(Request $request): array
    {
        if (!in_array('application/json;charset=utf-8', $request->getHeader('Content-Type'), true)) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Invalid request type.'
            ];
        }
        
        $queryParams = $request->getQueryParams();

        $orderBy = $this->orderBy;

        $collection = $this->adapter->findBy(
            [],
            $orderBy($queryParams['local'])
        );

        $this->status = 200;

        return [
            'status' => $this->status,
            'body' => $this->arrangeObjectsCollectionById($collection, true),
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
