<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Api;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllPaginatedApiAction extends Action
{
    protected $adapter;

    protected $findByCriteria;
    protected $orderBy;

    protected $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ConfigInterface $config
    ) {
        parent::__construct();

        $configValues = $config()['get_all_paginated'];

        $this->adapter = $adapter;

        $this->findByCriteria = $configValues['find_by_criteria'];
        $this->orderBy = $configValues['order_by'];
    }

    public function __invoke(Request $request, string $page): array
    {
        if (!in_array('application/json;charset=utf-8', $request->getHeader('Content-Type'), true)) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Invalid request type.'
            ];
        }

        $this->status = 200;

        $pagination = $request->getAttribute('pagination');

        $queryParams = $request->getQueryParams();

        $findByCriteria = $this->findByCriteria;
        $orderBy = $this->orderBy;

        $collection = $this->adapter->findBy(
            $findByCriteria(),
            $orderBy($queryParams['local']),
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'status' => $this->status,
            'body' => $this->arrangeObjectsCollectionById($collection, true),
            'pagination' => $pagination,
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
