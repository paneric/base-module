<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Api;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class UpdateMultipleApiAction extends Action
{
    protected $adapter;

    protected $daoClass;
    protected $dtoClass;
    protected $updateUniqueCriteria;
    protected $findByCriteria;

    protected $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ConfigInterface $config
    ) {
        parent::__construct();

        $configValues = $config()['update_multiple'];

        $this->adapter = $adapter;

        $this->daoClass = $configValues['dao_class'];
        $this->dtoClass = $configValues['dto_class'];
        $this->updateUniqueCriteria = $configValues['update_unique_criteria'];
        $this->findByCriteria = $configValues['find_by_criteria'];
    }

    public function __invoke(Request $request): ?array
    {
        if (!in_array('application/json;charset=utf-8', $request->getHeader('Content-Type'), true)) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Invalid request type.'
            ];
        }

        $this->status = 200;

        $collection = $this->createCollectionFromAttributes(
            $request->getParsedBody(),
            $this->daoClass,
            true
        );

        $validationReport = $request->getAttribute('validation');

        if (empty($validationReport[$this->dtoClass])) {
            $findByCriteria = $this->findByCriteria;
            $updateUniqueCriteria = $this->updateUniqueCriteria;

            $collection = $this->adapter->updateUniqueMultiple(
                $findByCriteria($collection),
                $updateUniqueCriteria($collection),
                $collection
            );

            if (empty($collection)) {
                return [
                    'status' => $this->status,
                ];
            }

            return [
                'status' => $this->status,
                'error' => 'db_update_multiple_unique_error',
            ];
        }

        return [
            'status' => $this->status,
            'error' => $validationReport,
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
