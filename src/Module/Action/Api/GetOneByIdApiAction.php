<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Api;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetOneByIdApiAction extends Action
{
    protected $adapter;

    protected $findOneByCriteria;

    protected $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ConfigInterface $config
    ) {
        parent::__construct();

        $configValues = $config()['get_one_by_id'];

        $this->adapter = $adapter;

        $this->findOneByCriteria = $configValues['find_one_by_criteria'];
    }

    public function __invoke(Request $request, string $id): ?array
    {
        if (!in_array('application/json;charset=utf-8', $request->getHeader('Content-Type'), true)) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Invalid request type.'
            ];
        }
        
        $findOneByCriteria = $this->findOneByCriteria;

        $dto = $this->adapter->findOneBy($findOneByCriteria($id));

        if ($dto ===  null) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Resource not found'
            ];
        }

        $this->status = 200;

        return [
            'status' => $this->status,
            'body' => $dto->convert(),
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
