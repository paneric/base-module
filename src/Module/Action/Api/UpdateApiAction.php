<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Api;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class UpdateApiAction extends Action
{
    protected $adapter;

    protected $daoClass;
    protected $dtoClass;
    protected $updateUniqueCriteria;
    protected $findOneByCriteria;

    protected $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ConfigInterface $config
    ) {
        parent::__construct();

        $configValues = $config()['update'];

        $this->adapter = $adapter;

        $this->daoClass = $configValues['dao_class'];
        $this->dtoClass = $configValues['dto_class'];
        $this->updateUniqueCriteria = $configValues['update_unique_criteria'];
        $this->findOneByCriteria = $configValues['find_one_by_criteria'];
    }

    public function __invoke(Request $request, string $id): ?array
    {
        if (!in_array('application/json;charset=utf-8', $request->getHeader('Content-Type'), true)) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Invalid request type.'
            ];
        }

        $findOneByCriteria = $this->findOneByCriteria;

        $validationReport = $request->getAttribute('validation');

        if (empty($validationReport[$this->dtoClass])) {
            $dao = new $this->daoClass();
            $dao->hydrate(
                $request->getParsedBody()
            );

            $updateUniqueCriteria = $this->updateUniqueCriteria;

            if ($this->adapter->updateUnique(
                    $findOneByCriteria($dao, $id),
                    $updateUniqueCriteria($id),
                    $dao
                ) !== null) {
                $this->status = 200;

                return [
                    'status' => $this->status,
                ];
            }

            $this->status = 200;

            return [
                'status' => $this->status,
                'error' => 'db_update_unique_error',
            ];
        }

        $this->status = 200;

        return [
            'status' => $this->status,
            'error' => $validationReport,
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
