<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Api;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class DeleteMultipleApiAction extends Action
{
    protected $adapter;

    protected $deleteByCriteria;
    protected $daoClass;
    protected $dtoClass;

    protected $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ConfigInterface $config
    ) {
        parent::__construct();

        $configValues = $config()['delete_multiple'];

        $this->adapter = $adapter;

        $this->daoClass = $configValues['dao_class'];
        $this->dtoClass = $configValues['dto_class'];
        $this->deleteByCriteria = $configValues['delete_by_criteria'];
    }

    public function __invoke(Request $request): ?array
    {
        if (!in_array('application/json;charset=utf-8', $request->getHeader('Content-Type'), true)) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Invalid request type.'
            ];
        }

        $this->status = 200;

        $collection = $this->createCollectionFromAttributes(
            $request->getParsedBody(),
            $this->daoClass,
            true
        );

        $validationReport = $request->getAttribute('validation');

        if (empty($validationReport[$this->dtoClass])) {
            $deleteByCriteria = $this->deleteByCriteria;

            $collection = $this->adapter->deleteMultiple(
                $deleteByCriteria($collection)
            );

            if (empty($collection)) {
                return [
                    'status' => $this->status,
                ];
            }

            return [
                'status' => $this->status,
                'error' => 'db_delete_multiple_error',
            ];
        }

        return [
            'status' => $this->status,
            'error' => $validationReport,
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
