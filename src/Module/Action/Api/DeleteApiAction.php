<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Api;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class DeleteApiAction extends Action
{
    protected $adapter;

    protected $deleteByCriteria;

    protected $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ConfigInterface $config
    ) {
        parent::__construct();

        $configValues = $config()['delete'];

        $this->adapter = $adapter;

        $this->deleteByCriteria = $configValues['delete_by_criteria'];
    }

    public function __invoke(Request $request, string $id): ?array
    {
        if (!in_array('application/json;charset=utf-8', $request->getHeader('Content-Type'), true)) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Invalid request type.'
            ];
        }

        $deleteByCriteria = $this->deleteByCriteria;

        if ($this->adapter->delete($deleteByCriteria(['id' => $id])) === 0) {
            $this->status = 400;

            return  [
                'status' => $this->status,
                'error' => 'Missing/invalid query parameter.'
            ];
        }

        $this->status = 200;

        return  [
            'status' => $this->status,
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
