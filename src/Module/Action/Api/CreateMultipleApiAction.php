<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Api;

use Paneric\CSRTriad\Action;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class CreateMultipleApiAction extends Action
{
    protected $adapter;

    protected $createUniqueCriteria;
    protected $daoClass;
    protected $dtoClass;

    protected $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ConfigInterface $config
    ) {
        parent::__construct();

        $configValues = $config()['create_multiple'];

        $this->adapter = $adapter;

        $this->daoClass = $configValues['dao_class'];
        $this->dtoClass = $configValues['dto_class'];
        $this->createUniqueCriteria = $configValues['create_unique_criteria'];
    }

    public function __invoke(Request $request): ?array
    {
        if (!in_array('application/json;charset=utf-8', $request->getHeader('Content-Type'), true)) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Invalid request type.'
            ];
        }

        $this->status = 200;

        $collection = $this->createCollectionFromAttributes(
            $request->getParsedBody(),
            $this->daoClass,
            true
        );

        $validationReport = $request->getAttribute('validation');

        if (empty($validationReport[$this->dtoClass])) {
            $createUniqueCriteria = $this->createUniqueCriteria;
            
            $collection = $this->adapter->createUniqueMultiple(
                $createUniqueCriteria($collection),
                $collection
            );

            if (empty($collection)) {
                return [
                    'status' => $this->status,
                ];
            }

            return [
                'status' => $this->status,
                'error' => 'db_create_multiple_error',
                'body' => $this->convertCollection($collection),
            ];
        }

        return [
            'status' => $this->status,
            'error' => $validationReport,
            'body' => $this->convertCollection($collection),
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
