<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Apc;

use Paneric\CSRTriad\Action;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Slim\Exception\HttpBadRequestException;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetOneByIdApcAction extends Action
{
    protected $manager;

    protected $baseUrl;
    protected $uri;
    protected $moduleNameSc;
    protected $prefix;

    public function __construct(
        HttpClientManager $manager,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['get_one_by_id'];

        $this->manager = $manager;

        $this->baseUrl = $configValues['base_url'];
        $this->uri = $configValues['api-lc.get'];
        $this->moduleNameSc = $configValues['module_name_sc'];
        $this->prefix = $configValues['prefix'];
    }

    public function __invoke(Request $request, string $id): ?array
    {
        $this->session->setFlash(
            ['module_name_sc' => $this->moduleNameSc],
            'value'
        );

        $options = [
            'headers' => [
                'Content-Type' => 'application/json;charset=utf-8',
                'Authorization' => ' Bearer ' . $request->getAttribute('token'),
            ],
            'query' => [
                'local' => strtolower($this->session->getData('local')),
            ],
        ];

        $jsonResponse = $this->manager->getJsonResponse(
            'GET',
            sprintf(
                '%s%s%s',
                $this->baseUrl,
                $this->uri,// /api/bc/
                $id
            ),
            $options
        );

        if ($jsonResponse['status'] === 200) {
            return [
                $this->prefix => $jsonResponse['body']
            ];
        }

        try {
            if ($jsonResponse['status'] === 400) {
                throw new HttpBadRequestException(
                    $jsonResponse['error']
                );
            }
        } catch (HttpBadRequestException $e) {
            error_log(sprintf(
                "%s%s%s%s",
                $e->getFile() . "\n",
                $e->getLine() . "\n",
                $e->getMessage() . "\n",
                $e->getTraceAsString() . "\n"
            ), 0);
        }

        return null;
    }
}
