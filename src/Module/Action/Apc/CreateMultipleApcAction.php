<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Apc;

use Paneric\CSRTriad\Action;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Slim\Exception\HttpBadRequestException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class CreateMultipleApcAction extends Action
{
    protected $manager;

    protected $baseUrl;
    protected $uri;
    protected $moduleNameSc;
    protected $prefix;

    public function __construct(
        HttpClientManager $manager,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['create_multiple'];

        $this->manager = $manager;

        $this->baseUrl = $configValues['base_url'];
        $this->uri = $configValues['api-lcs.create'];
        $this->moduleNameSc = $configValues['module_name_sc'];
        $this->daoClass = $configValues['dao_class'];
        $this->prefix = $configValues['prefix'];
    }
    
    public function __invoke(Request $request): ?array
    {
        $attributes = [];

        if ($request->getMethod() === 'POST') {
            $attributes = $request->getParsedBody();

            $options = [
                'headers' => [
                    'Content-Type' => 'application/json;charset=utf-8',
                    'Authorization' => ' Bearer ' . $request->getAttribute('token'),
                ],
                'json' => $attributes,
            ];

            $jsonResponse = $this->manager->getJsonResponse(
                'POST',
                sprintf(
                    '%s%s',
                    $this->baseUrl,
                    $this->uri
                ),
                $options
            );

            if (!isset($jsonResponse['error'])) {
                return null;
            }

            if ($jsonResponse['status'] === 200) {
                $this->session->setFlash(
                    ['module_name_sc' => $this->moduleNameSc],
                    'value'
                );

                if (is_array($jsonResponse['error'])) {
                    return [
                        $this->prefix . 's' => $jsonResponse['body'],
                        'report' => $jsonResponse['error'],
                    ];
                }

                $this->session->setFlash(
                    [$jsonResponse['error']],
                    'error'
                );

                return [
                    $this->prefix . 's' => $jsonResponse['body'],
                ];
            }

            try {
                if ($jsonResponse['status'] === 400) {
                    throw new HttpBadRequestException(
                        $jsonResponse['error']
                    );
                }
            } catch (HttpBadRequestException $e) {
                error_log(sprintf(
                    "%s%s%s%s",
                    $e->getFile() . "\n",
                    $e->getLine() . "\n",
                    $e->getMessage() . "\n",
                    $e->getTraceAsString() . "\n"
                ), 0);
            }
        }
        
        $this->session->setFlash(
            ['module_name_sc' => $this->moduleNameSc],
            'value'
        );

        return [
            $this->prefix . 's' => [],
        ];
    }
}
