<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Apc;

use Paneric\CSRTriad\Action;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllPaginatedApcAction extends Action
{
    protected $manager;

    protected $baseUrl;
    protected $uri;
    protected $moduleNameSc;
    protected $prefix;

    public function __construct(
        HttpClientManager $manager,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['get_all_paginated'];

        $this->manager = $manager;

        $this->baseUrl = $configValues['base_url'];
        $this->uri = $configValues['api-lcs.get.page'];
        $this->originUri = $configValues['apc-lcs.show-all-paginated'];
        $this->moduleNameSc = $configValues['module_name_sc'];
        $this->prefix = $configValues['prefix'];
    }

    public function __invoke(Request $request, string $page = null): array
    {
        $this->session->setFlash(
            ['module_name_sc' => $this->moduleNameSc],
            'value'
        );

        $options = [
            'headers' => [
                'Content-Type' => 'application/json;charset=utf-8',
                'Authorization' => ' Bearer ' . $request->getAttribute('token'),
            ],
            'query' => [
                'local' => strtolower($this->session->getData('local')),
            ],
        ];

        $jsonResponse = $this->manager->getJsonResponse(
            'GET',
            sprintf(
                '%s%s%s',
                $this->baseUrl,
                $this->uri,
                $page ?? '1'
            ),
            $options
        );

        $jsonResponse['pagination']['path'] = $this->originUri;

        if ($jsonResponse['status'] === 200) {
            $this->session->setData($jsonResponse['pagination'], 'pagination');

            return [
                $this->prefix . 's' =>$jsonResponse['body']
            ];
        }

        return [];
    }
}
