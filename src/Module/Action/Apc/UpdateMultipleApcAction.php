<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Apc;

use Paneric\CSRTriad\Action;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Slim\Exception\HttpBadRequestException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class UpdateMultipleApcAction extends Action
{
    protected $manager;

    protected $baseUrl;
    protected $uri;
    protected $originUri;
    protected $moduleNameSc;

    public function __construct(
        HttpClientManager $manager,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['update_multiple'];

        $this->manager = $manager;

        $this->baseUrl = $configValues['base_url'];
        $this->uri = $configValues['api-lcs.update'];
        $this->originUri = $configValues['apc-lcs.edit'];
        $this->moduleNameSc = $configValues['module_name_sc'];
    }

    public function __invoke(Request $request): ?array
    {
        if ($request->getMethod() === 'POST') {
            $options = [
                'headers' => [
                    'Content-Type' => 'application/json;charset=utf-8',
                    'Authorization' => ' Bearer ' . $request->getAttribute('token'),
                ],
                'json' => $request->getParsedBody(),
            ];

            $jsonResponse = $this->manager->getJsonResponse(
                'PUT',
                sprintf(
                    '%s%s',
                    $this->baseUrl,
                    $this->uri
                ),
                $options
            );

            if (!isset($jsonResponse['error'])) {
                return null;
            }
            
            if ($jsonResponse['status'] === 200) {
                $this->session->setFlash(
                    ['module_name_sc' => $this->moduleNameSc],
                    'value'
                );

                if (is_array($jsonResponse['error'])) {
                    return [
                        'report' => $jsonResponse['error']
                    ];
                }

                $this->session->setFlash(
                    [$jsonResponse['error']],
                    'error'
                );

                return [];
            }

            try {
                if ($jsonResponse['status'] === 400) {
                    throw new HttpBadRequestException(
                        $jsonResponse['error']
                    );
                }
            } catch (HttpBadRequestException $e) {
                error_log(sprintf(
                    "%s%s%s%s",
                    $e->getFile() . "\n",
                    $e->getLine() . "\n",
                    $e->getMessage() . "\n",
                    $e->getTraceAsString() . "\n"
                ), 0);
            }
        }

        $this->session->setFlash(
            ['module_name_sc' => $this->moduleNameSc],
            'value'
        );

        return [];
    }

    public function adaptPaginationPath(): void
    {
        $pagination = $this->session->getData('pagination');
        $pagination['path'] = $this->originUri;
        $this->session->setData($pagination, 'pagination');
    }
}
