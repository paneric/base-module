<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Action\Apc;

use Paneric\CSRTriad\Action;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Slim\Exception\HttpBadRequestException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class CreateApcAction extends Action
{
    protected $manager;
    
    protected $baseUrl;
    protected $uri;
    protected $moduleNameSc;
    protected $prefix;

    public function __construct(
        HttpClientManager $manager,
        SessionInterface $session,
        ConfigInterface $config
    ) {
        parent::__construct($session);

        $configValues = $config()['create'];

        $this->manager = $manager;
        
        $this->baseUrl = $configValues['base_url'];
        $this->uri = $configValues['api-lc.create'];
        $this->moduleNameSc = $configValues['module_name_sc'];
        $this->prefix = $configValues['prefix'];
    }

    public function __invoke(Request $request): ?array
    {
        if ($request->getMethod() === 'POST') {
            $options = [
                'headers' => [
                    'Content-Type' => 'application/json;charset=utf-8',
                    'Authorization' => ' Bearer ' . $request->getAttribute('token'),
                ],
                'json' => $request->getParsedBody(),
            ];

            $jsonResponse = $this->manager->getJsonResponse(
                'POST',
                sprintf(
                    '%s%s',
                    $this->baseUrl,
                    $this->uri
                ),
                $options
            );

            if (!isset($jsonResponse['error'])) {
                return null;
            }

            if ($jsonResponse['status'] === 200) {
                $this->session->setFlash(
                    ['module_name_sc' => $this->moduleNameSc],
                    'value'
                );

                if (is_array($jsonResponse['error'])) {
                    return [
                        $this->prefix => $jsonResponse['body'],
                        'report' => $jsonResponse['error']
                    ];
                }

                $this->session->setFlash(
                    ['db_add_' . $this->moduleNameSc . '_unique_error'],
                    'error'
                );

                return [
                    $this->prefix => $jsonResponse['body']
                ];
            }

            try {
                if ($jsonResponse['status'] === 400) {
                    throw new HttpBadRequestException(
                        $jsonResponse['error']
                    );
                }
            } catch (HttpBadRequestException $e) {
                error_log(sprintf(
                    "%s%s%s%s",
                    $e->getFile() . "\n",
                    $e->getLine() . "\n",
                    $e->getMessage() . "\n",
                    $e->getTraceAsString() . "\n"
                ), 0);
            }
        }

        $this->session->setFlash(
            ['module_name_sc' => $this->moduleNameSc],
            'value'
        );

        return [
            $this->prefix => [],
        ];
    }
}
