<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Repository;

use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\HttpClient\HttpClientAdapter;

class ModuleHttpClient extends HttpClientAdapter implements ModuleRepositoryInterface {}
