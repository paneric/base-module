<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Module\Repository;

use Paneric\DBAL\Manager;
use Paneric\DBAL\Repository;
use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;
use Paneric\Interfaces\Config\ConfigInterface;

class ModuleRepository extends Repository implements ModuleRepositoryInterface
{
    public function __construct(Manager $manager, ConfigInterface $config)
    {
        parent::__construct($manager);

        $configValues = $config();

        $this->table = $configValues['table'];
        $this->daoClass = $configValues['dao_class'];
        $this->fetchMode = $configValues['fetch_mode'];

        $this->createUniqueWhere = $configValues['create_unique_where'];
        $this->updateUniqueWhere = $configValues['update_unique_where'];
    }
}
