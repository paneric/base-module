<?php

declare(strict_types=1);

namespace Paneric\BaseModule\Interfaces\Repository;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface ModuleRepositoryInterface
{
    /**
     * @return null|DataObjectInterface|array
     */
    public function findOneBy(array $criteria);

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;

    public function createUnique(array $criteria, DataObjectInterface $dataObject): ?string;
    public function createUniqueMultiple(array $criteria, array $dataObjects): array;

    public function updateUnique(array $criteriaSelect, array $criteriaUpdate, DataObjectInterface $dataObject): ?int;
    public function updateUniqueMultiple(array $criteriaSelect, array $criteriaUpdate, array $dataObjects): array;

    public function delete(array $criteria): int;
    public function deleteMultiple(array $criteriaDelete): array;
}
